﻿namespace G2O_Launcher
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Navigation;
    using System.Windows.Shapes;
    using ViewModel;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            // Bind View Models
            var mainViewModel = new MainWindowViewModel();
            mainViewModel.FavoriteVM = new FavoriteViewModel(FavoriteView.ServerView.Description);
            mainViewModel.InternetVM = new InternetViewModel(InternetView.ServerView.Description);

            DataContext = mainViewModel;
            FavoriteView.DataContext = mainViewModel.FavoriteVM;
            InternetView.DataContext = mainViewModel.InternetVM;

            // Bind Context Menus
            FavoriteView.ServerView.ContextMenu = (ContextMenu)FavoriteView.Resources["Menu"];
            InternetView.ServerView.ContextMenu = (ContextMenu)InternetView.Resources["Menu"];
        }
    }
}
