﻿namespace G2O_Launcher.ViewModel
{
    using System;
    using System.Collections.ObjectModel;
    using System.Collections.Generic;
    using System.Windows;
    using System.Diagnostics;
    using System.Reflection;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using System.Data;
    using System.Threading;
    using System.Windows.Threading;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Text.RegularExpressions;
    using G2O_Launcher.Network;
    using G2O_Launcher.ModelView;
    using G2O_Launcher.Update;
    using G2O_Launcher.Launcher;
    using G2O_Launcher.Utils;

    class FavoriteViewModel : ServerViewModel
    {
        private const float TIMEOUT = 2.5f;
        private const int DEFAULT_PORT = 28970;
        private const string FAVORITE_PATH = "favorite.xml";

        public ICommand AddCommand { get; private set; }
        public ICommand RemoveCommand { get; private set; }

        private DispatcherTimer _TimeoutTimer;
        private ServersDict _FavServers;
        // public event

        public FavoriteViewModel(WebBrowser description) :
            base(description)
        {
            _FavServers = new ServersDict();

            _TimeoutTimer = new DispatcherTimer();
            _TimeoutTimer.Interval = TimeSpan.FromSeconds(TIMEOUT);
            _TimeoutTimer.Tick += Timeout;

            RegisterCommands();

            LoadXml();
        }

        ~FavoriteViewModel()
        {
            SaveXml();
        }

        private void RegisterCommands()
        {
            AddCommand = new RelayCommand(AddServer_Execute);
            RemoveCommand = new RelayCommand(RemoveServer_Execute);
        }

        private void Timeout(object sender, EventArgs e)
        {
            foreach (var server in _FavServers.Servers)
                if (!server.Reached) Servers.Add(new ServerNotFound(server.Host, server.Port));

            _TimeoutTimer.Stop();
        }

        private void LoadXml()
        {
            FavoriteXml.Parse(FAVORITE_PATH).ForEach((server) => { _FavServers.Add(server); Servers.Add(server); });
        }

        private void SaveXml()
        {
            FavoriteXml.Generate(FAVORITE_PATH, _FavServers.Servers.ToList<Server>());
        }

        /////////////////////////////////////////////////
        /// Events
        /////////////////////////////////////////////////

        protected override void Listener_ServerInfo(object sender, ServerInfoEventArgs args)
        {
            var server = _FavServers.GetByIP(args.IP, args.Port);
            if (server == null) return;
            
            TimeSpan elapsed = DateTime.Now.Subtract(LastRefresh);

            server.Hostname = args.Hostname;
            server.Slots = string.Format("{0}/{1}", args.Players, args.MaxPlayers);
            server.Version = args.Version;
            server.Ping = elapsed.Milliseconds;
            server.Reached = true;

            Servers.Add(server);
        }

        /////////////////////////////////////////////////
        /// Commands
        /////////////////////////////////////////////////


        protected override void Refresh_Execute(object value)
        {
            Refresh(_FavServers.Servers.ToList<ServerBase>());
            _TimeoutTimer.Start();
        }

        protected override bool Refresh_CanExecute(object value)
        {
            return !_TimeoutTimer.IsEnabled;
        }

        protected virtual void AddServer_Execute(object value)
        {
            if (value != null)
            {
                string hostName = value.ToString();
                int port = DEFAULT_PORT;

                Regex reIpPort = new Regex(@"^([a-zA-Z0-9\.\-]+)\:(\d{1,5})$");
                Match match = reIpPort.Match(hostName);

                if (!match.Success)
                {
                    MessageBox.Show("Wrong server format! Should be host:port.");
                    return;
                }

                hostName = match.Groups[1].Value;
                port = int.Parse(match.Groups[2].Value);

                Server server = new ServerNotFound(hostName, port);
                _FavServers.Add(server);
                Servers.Add(server);
            }
        }

        protected virtual void RemoveServer_Execute(object value)
        {
            _FavServers.Remove(SelectedServer);
            Servers.Remove(SelectedServer);
        }
    }
}
