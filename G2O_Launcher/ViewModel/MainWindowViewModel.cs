﻿namespace G2O_Launcher.ViewModel
{
    using System;
    using System.Windows;
    using System.Diagnostics;
    using System.Reflection;
    using System.Collections.Generic;
    using System.IO;
    using ModelView;
    using Update;
    using G2O;

    class MainWindowViewModel : BaseViewModel
    {
        private enum Tab
        {
            INTERNET,
            FAVORITE
        }

        private int _ProgressValue;
        private Visibility _ProgressVisible;
        private string _VersionText;
        private Visibility _VersionVisible;
        private int _TabIndex;

        private Updater _Updater;

        public FavoriteViewModel FavoriteVM;
        public InternetViewModel InternetVM;

        public MainWindowViewModel()
        {
            InitModelViewVar();
            RegisterEvents();

            _Updater.Check();
        }

        ~MainWindowViewModel()
        {
            SaveNickname();
        }

        private void InitModelViewVar()
        {
            // Progress bar
            _ProgressValue = 0;
            _ProgressVisible = Visibility.Hidden;

            // Tab control
            _TabIndex = 0;

            try
            {
                // Version
                using (var proxy = new Proxy())
                {
                    G2O.Version version = proxy.GetVersion();

                    _VersionText = "Version: " + version;
                    _VersionVisible = Visibility.Visible;
                }

                // Updater
                _Updater = new Updater();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                System.Environment.Exit(1);
            }     
        }

        private void RegisterEvents()
        {
            _Updater.UpdateResult += Update_ResponseResult;
            _Updater.DownloadCompleted += Update_DownloadCompleted;
            _Updater.DownloadProgress += (obj, args) => { UpdateProgress = args.Progress; };
        }

        private void SaveNickname()
        {
            switch ((Tab)_TabIndex)
            {
                case Tab.INTERNET:
                    RegistryConfig.Nickname = InternetVM.Nickname;
                    break;

                case Tab.FAVORITE:
                    RegistryConfig.Nickname = FavoriteVM.Nickname;
                    break;
            }
        }

        public int UpdateProgress
        {
            get
            {
                return _ProgressValue;
            }

            set
            {
                if (value != _ProgressValue)
                {
                    _ProgressValue = value;
                    OnPropertyChanged("UpdateProgress");
                }
            }
        }

        public Visibility UpdateVisible
        {
            get
            {
                return _ProgressVisible;
            }

            set
            {
                if (value != _ProgressVisible)
                {
                    _ProgressVisible = value;
                    OnPropertyChanged("UpdateVisible");
                }
            }
        }

        public string VersionText
        {
            get
            {
                return _VersionText;
            }

            set
            {
                if (value != _VersionText)
                {
                    _VersionText = value;
                    OnPropertyChanged("VersionText");
                }
            }
        }

        public Visibility VersionVisible
        {
            get
            {
                return _VersionVisible;
            }

            set
            {
                if (value != _VersionVisible)
                {
                    _VersionVisible = value;
                    OnPropertyChanged("VersionVisible");
                }
            }
        }

        public int TabIndex
        {
            get
            {
                return _TabIndex;
            }

            set
            {
                if (value != _TabIndex)
                {
                    switch ((Tab)value)
                    {
                        case Tab.INTERNET:
                            InternetVM.Nickname = FavoriteVM.Nickname;
                            break;

                        case Tab.FAVORITE:
                            FavoriteVM.Nickname = InternetVM.Nickname;
                            break;
                    }

                    _TabIndex = value;
                    OnPropertyChanged("TabIndex");
                }
            }
        }

        private void Update_DownloadCompleted(object sender, UpdateCompleteEventArgs args)
        {
            UpdateVisible = Visibility.Hidden;
            VersionVisible = Visibility.Visible;

            // Run update script
            string appName = Assembly.GetExecutingAssembly().GetName().Name + ".exe";

            using (var batFile = new StreamWriter(File.Create("Update.bat")))
            {
                batFile.WriteLine("@ECHO OFF");
                batFile.WriteLine("TIMEOUT /t 1 /nobreak > NUL");
                batFile.WriteLine("TASKKILL /IM \"{0}\" > NUL", appName);
                args.Files.ForEach((file) => batFile.WriteLine("MOVE \"{0}{1}\" \"{0}\"", file, Updater.UpdateExt));
 
                batFile.WriteLine("DEL \"%~f0\" & START \"\" /B \"{0}\"", appName);
            }

            ProcessStartInfo startInfo = new ProcessStartInfo("Update.bat");
            // Hide the terminal window
            startInfo.CreateNoWindow = true;
            startInfo.UseShellExecute = false;
            startInfo.WorkingDirectory = AppDomain.CurrentDomain.BaseDirectory;
            Process.Start(startInfo);

            Environment.Exit(0);
        }

        private void Update_ResponseResult(object sender, UpdateResponseEventArgs args)
        {
            args.Filename = "test.zip";

            if (args.Code != UpdateStatus.OUTDATED)
            {
                if (args.Code == UpdateStatus.NOT_SUPPORTED)
                {
                    MessageBoxResult result = MessageBox.Show(
                        "This version is no longer supported!\nPlease visit out website to download new version.",
                        "New version", MessageBoxButton.YesNo
                    );
                    
                    if (result == MessageBoxResult.Yes) Process.Start(args.Link);
                }
            }
            else
            {
                UpdateVisible = Visibility.Visible;
                UpdateProgress = 0;

                VersionVisible = Visibility.Hidden;
                VersionText = string.Format("Updating to version: {0}.{1}.{2}.{3}", args.Major, args.Minor, args.Patch, args.Build);

                _Updater.Download(args.Link, args.Filename);
            }
        }
    }
}
