﻿namespace G2O_Launcher.ViewModel
{
    using System;
    using System.Collections.ObjectModel;
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Threading;
    using System.Windows.Input;
    using System.Diagnostics;
    using System.Reflection;
    using System.Threading;
    using System.IO;
    using G2O_Launcher.Utils;
    using G2O_Launcher.Network;
    using G2O_Launcher.ModelView;
    using G2O_Launcher.G2O;
    using G2O_Launcher.Update;
    using G2O_Launcher.Launcher;

    class ServerViewModel : BaseViewModel
    {
        private const float TIMEOUT = 2.0f;

        public ObservableCollection<Server> Servers { get; set; }
        public ICommand RefreshCommand { get; private set; }
        public ICommand JoinCommand { get; private set; }

        private ServerListener _Listener;
        private DispatcherTimer _PingTimer;
        private WebBrowser _Description;
        private DateTime _LastRefresh;
        private DateTime _LastPing;
        private Server _SelectedServer;
        private string _Nickname;
        private string _ServerWorld;
        private string _ServerDescription;

        public ServerViewModel(WebBrowser description)
        {
            Servers = new ObservableCollection<Server>();

            _PingTimer = new DispatcherTimer();
            _PingTimer.Interval = TimeSpan.FromSeconds(TIMEOUT);
            _PingTimer.Tick += _Ping;
            _PingTimer.Start();

            _Description = description;
            _ServerWorld = "";
            _ServerDescription = "";
            _Nickname = RegistryConfig.Nickname;

            _Listener = new ServerListener();
            _Listener.Start();

            RegisterCommands();
            RegisterEvents();
        }

        private void RegisterEvents()
        {
            _Listener.ServerPing += Listener_ServerPing;
            _Listener.ServerInfo += Listener_ServerInfo;
            _Listener.ServerDetails += Listener_ServerDetails;
        }

        private void RegisterCommands()
        {
            RefreshCommand = new RelayCommand(Refresh_Execute, Refresh_CanExecute);
            JoinCommand = new RelayCommand(Join_Execute, Join_CanExecute);
        }

        private void _Ping(object sender, EventArgs e)
        {
            if (SelectedServer != null && _SelectedServer.Reached)
            {
                _Listener.Send(PacketPing.Identifier, SelectedServer);
                _LastPing = DateTime.Now;
            }
        }

        public virtual void Refresh(List<ServerBase> servers)
        {
            Servers.Clear();

            servers.ForEach((server) =>
            {
                _Listener.Send(PacketInfo.Identifier, server);
                server.Reached = false;
            });

            _LastRefresh = DateTime.Now;
        }

        public DateTime LastRefresh
        {
            get
            {
                return _LastRefresh;
            }
        }

        public DateTime LastPing
        {
            get
            {
                return _LastPing;
            }
        }

        public string Nickname
        {
            get
            {
                return _Nickname;
            }

            set
            {
                if (value.Length < 21 && _Nickname != value)
                {
                    _Nickname = value;
                    OnPropertyChanged("Nickname");
                }
            }
        }

        public Server SelectedServer
        {
            get
            {
                return _SelectedServer;
            }

            set
            {
                if (_SelectedServer != value)
                {
                    _SelectedServer = value;
                    OnPropertyChanged("SelectedItem");

                    // Get server detailed info
                    if (_SelectedServer != null)
                    {
                        if (_SelectedServer.Reached)
                            _Listener.Send(PacketDetails.Identifier, _SelectedServer);

                        // Refresh ping time
                        _LastPing = DateTime.Now;

                        // Clear old detailed info
                        ServerWorld = "";
                        ServerDescription = "";
                    }
                }
            }
        }

        public string ServerWorld
        {
            get
            {
                return _ServerWorld;
            }

            set
            {
                if (_ServerWorld != value)
                {
                    _ServerWorld = value;
                    OnPropertyChanged("ServerWorld");
                }
            }
        }

        public string ServerDescription
        {
            get
            {
                return _ServerDescription;
            }

            set
            {
                if (_ServerDescription != value)
                {
                    if (!string.IsNullOrEmpty(value))
                        _Description.NavigateToString(value);
                    else
                        _Description.Navigate("about:blank");

                    _ServerDescription = value;
                    OnPropertyChanged("ServerDescription");
                }
            }
        }

        /////////////////////////////////////////////////
        /// Events
        /////////////////////////////////////////////////

        protected virtual void Listener_ServerPing(object sender, ServerPingEventArgs args)
        {
            TimeSpan elapsed = DateTime.Now.Subtract(LastPing);
            SelectedServer.Ping = elapsed.Milliseconds;
        }

        protected virtual void Listener_ServerInfo(object sender, ServerInfoEventArgs args)
        {
            TimeSpan elapsed = DateTime.Now.Subtract(LastRefresh);

            Server server = new Server();
            server.Host = args.IP;
            server.Port = args.Port;
            server.Hostname = args.Hostname;
            server.Slots = string.Format("{0}/{1}", args.Players, args.MaxPlayers);
            server.Version = args.Version;
            server.Ping = elapsed.Milliseconds;
            server.Reached = true;

            Servers.Add(server);
        }

        protected virtual void Listener_ServerDetails(object sender, ServerDetailsEventArgs args)
        {
            ServerWorld = args.World;
            ServerDescription = args.Description;
        }

        /////////////////////////////////////////////////
        /// Commands
        /////////////////////////////////////////////////

        protected virtual void Refresh_Execute(object value)
        {
        }

        protected virtual bool Refresh_CanExecute(object value)
        {
            return true;
        }

        protected virtual void Join_Execute(object value)
        {
            G2O.Version version = G2O.Version.Parse(SelectedServer.Version);
            if (version == null)
            {
                MessageBox.Show("Unsupported version!");
                return;
            }

            if (Nickname.Length == 0)
            {
                MessageBox.Show("Nickname field is empty!");
                return;
            }

            RegistryConfig.Nickname = Nickname;
            RegistryConfig.IpPort = string.Format("{0}:{1}", SelectedServer.Host, SelectedServer.Port);
            if (!string.IsNullOrEmpty(ServerWorld)) RegistryConfig.World = ServerWorld;

            using (var proxy = new Proxy())
            {
                Proxy.RunResult result = proxy.Run(version);

                switch (result)
                {
                    case Proxy.RunResult.MISSING_VERSION:
                        MessageBox.Show("Cannot join to server! You don't have required version.");
                        break;

                    case Proxy.RunResult.GOTHIC_NOT_FOUNT:
                        MessageBox.Show("Could not open Gothic2.exe.\nDid you install G2O to a folder with Gothic 2: Night of the Raven?");
                        break;

                    case Proxy.RunResult.UNKNOW:
                        MessageBox.Show(String.Format("Could not start Gothic 2 Online {0}", version));
                        break;
                }
            }                
        }

        protected virtual bool Join_CanExecute(object value)
        {
            return SelectedServer != null && SelectedServer.Reached;
        }
    }
}
