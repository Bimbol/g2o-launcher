﻿namespace G2O_Launcher.ViewModel
{
    using System;
    using System.Collections.Generic;
    using G2O_Launcher.Launcher;

    class PublicServersEventArgs : EventArgs
    {
        public List<ServerBase> Servers;

        public PublicServersEventArgs(List<ServerBase> servers)
        {
            Servers = servers;
        }
    }
}
