﻿namespace G2O_Launcher.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Windows;
    using System.Diagnostics;
    using System.Reflection;
    using System.IO;
    using System.Windows.Threading;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Text;
    using System.Net;
    using System.Net.Http;
    using Newtonsoft.Json;
    using G2O_Launcher.Network;
    using G2O_Launcher.ModelView;
    using G2O_Launcher.Update;
    using G2O_Launcher.Launcher;

    class InternetViewModel : ServerViewModel
    {
        private const float TIMEOUT = 5.0f;
        private readonly string MasterUrl = "http://185.5.97.181:8000/master/public_servers";

        public event EventHandler<PublicServersEventArgs> PublicServers;

        private DispatcherTimer _TimeoutTimer;

        public InternetViewModel(WebBrowser description)
            : base(description)
        {
            _TimeoutTimer = new DispatcherTimer();
            _TimeoutTimer.Interval = TimeSpan.FromSeconds(TIMEOUT);
            _TimeoutTimer.Tick += _Timeout;

            RegisterEvents();
        }

        private void RegisterEvents()
        {
            PublicServers += PublicServers_Response;
        }

        private void _Timeout(object sender, EventArgs e)
        {
            _TimeoutTimer.Stop();
        }

        private async void FetchServers()
        {
            if (PublicServers == null) return;

            try
            {
                using (var httpClient = new HttpClient())
                {
                    var responseHttp = await httpClient.GetAsync(MasterUrl);
                    var responseBody = await responseHttp.Content.ReadAsStringAsync();
 
                    var result = JsonConvert.DeserializeObject<List<ServerBase>>(responseBody);
                    this.PublicServers.Invoke(this, new PublicServersEventArgs(result));
                }
            }
            catch (Exception)
            {
                this.PublicServers.Invoke(this, new PublicServersEventArgs(null));
            }
        }

        private void PublicServers_Response(object sender, PublicServersEventArgs args)
        {
            if (args.Servers == null)
            {
                MessageBox.Show("Cannot connect or fetch data from the Master Server!");
                _TimeoutTimer.Stop();
            }
            else
                Refresh(args.Servers);
        }

        protected override void Refresh_Execute(object value)
        {
            FetchServers();
            _TimeoutTimer.Start();
        }

        protected override bool Refresh_CanExecute(object value)
        {
            return !_TimeoutTimer.IsEnabled;
        }
    }
}
