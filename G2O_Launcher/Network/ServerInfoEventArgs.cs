﻿namespace G2O_Launcher.Network
{
    class ServerInfoEventArgs : ServerEventArgs
    {
        public string Hostname;
        public int Players;
        public int MaxPlayers;
        public string Version;
    }
}
