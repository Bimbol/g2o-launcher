﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G2O_Launcher.Network
{
    interface IPacket
    {
        bool parse(byte[] data, int length);
    }
}
