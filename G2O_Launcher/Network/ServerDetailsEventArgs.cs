﻿namespace G2O_Launcher.Network
{
    class ServerDetailsEventArgs : ServerEventArgs
    {
        public string World;
        public string Description;
    }
}
