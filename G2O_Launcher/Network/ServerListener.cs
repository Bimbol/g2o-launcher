﻿namespace G2O_Launcher.Network
{
    using System;
    using System.Text;
    using System.Net;
    using System.Linq;
    using System.Net.Sockets;
    using Launcher;

    class ServerListener
    {
        public event EventHandler<ServerPingEventArgs> ServerPing;
        public event EventHandler<ServerInfoEventArgs> ServerInfo;
        public event EventHandler<ServerDetailsEventArgs> ServerDetails;

        private Socket _Socket;
        private EndPoint _Client;
        private byte[] _Buffer;
        private bool _Running;

        public ServerListener()
        {
            uint IOC_IN = 0x80000000;
            uint IOC_VENDOR = 0x18000000;
            uint SIO_UDP_CONNRESET = IOC_IN | IOC_VENDOR | 12;

            _Socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp) { Blocking = false };
            _Socket.IOControl((IOControlCode)SIO_UDP_CONNRESET, new byte[] { 0, 0, 0, 0 }, null); // Disable "Connection Reset" exception

            _Client = new IPEndPoint(System.Net.IPAddress.Any, 0);
            _Buffer = new byte[512];

            _Running = false;
        }

        public void Start()
        {
            if (_Running) return;

            _Socket.Bind(new System.Net.IPEndPoint(System.Net.IPAddress.Any, 0));
            _Socket.BeginReceiveMessageFrom(_Buffer, 0, _Buffer.Length, SocketFlags.None, ref _Client, OnReceive, _Socket);

            _Running = true;
        }

        public void Stop()
        {
            if (!_Running) return;

            _Socket.Close();
            _Running = false;
        }

        public async void Send(string buffer, ServerBase server)
        {
            try
            {
                IPAddress ipAddr = null;
                if (server.Host != null)
                {
                    IPAddress[] addresses = await Dns.GetHostAddressesAsync(server.Host);

                    ipAddr = addresses.First(ip => ip.AddressFamily == AddressFamily.InterNetwork);
                    server.IP = ipAddr.ToString();
                }
                else
                {
                    server.Host = server.IP;
                    ipAddr = IPAddress.Parse(server.IP);
                }

                byte[] byteBuffer = Encoding.ASCII.GetBytes(buffer);
                IPEndPoint endPoint = new IPEndPoint(ipAddr, server.Port);

                _Socket.BeginSendTo(byteBuffer, 0, byteBuffer.Length, SocketFlags.None, endPoint, OnSend, null);
            }
            catch (SocketException) { }
            catch (FormatException) { }
            catch (ArgumentNullException) { }
        }

        private void HandlePacket(byte[] data, int length, EndPoint endPoint)
        {
            // Get 3 first bytes as header
            string header = Encoding.ASCII.GetString(data, 0, 3);
            
            switch (header)
            {
                case PacketPing.Identifier:
                    OnPacketPing(data, length, endPoint);
                    break;

                case PacketInfo.Identifier:
                    OnPacketInformation(data, length, endPoint);
                    break;

                case PacketDetails.Identifier:
                    OnPacketDetails(data, length, endPoint);
                    break;
            }
        }

        private void OnReceive(System.IAsyncResult result)
        {
            EndPoint remoteEnd = new IPEndPoint(IPAddress.Any, 0);

            try
            {
                IPPacketInformation packetInfo;
                SocketFlags flags = SocketFlags.None;
                Socket recvSocket = (Socket)result.AsyncState;

                int readBytes = recvSocket.EndReceiveMessageFrom(result, ref flags, ref remoteEnd, out packetInfo);
                if (readBytes > 0) HandlePacket(_Buffer, readBytes, remoteEnd);
            }
            catch (SocketException)
            {
                // Nothing to do
            }

            _Socket.BeginReceiveMessageFrom(_Buffer, 0, _Buffer.Length, SocketFlags.None, ref _Client, OnReceive, _Socket);
        }

        private void OnSend(System.IAsyncResult result)
        {
            _Socket.EndSendTo(result);
        }

        private void OnPacketPing(byte[] data, int length, EndPoint endPoint)
        {
            if (ServerPing == null) return;

            try
            {
                string[] address = endPoint.ToString().Split(':');

                App.Current.Dispatcher.Invoke(new Action(() => ServerPing.Invoke(this, new ServerPingEventArgs()
                {
                    IP = address[0],
                    Port = int.Parse(address[1])
                })));
            }
            catch (Exception)
            {
                // Just ignore
            }
        }

        private void OnPacketInformation(byte[] data, int length, EndPoint endPoint)
        {
            if (ServerInfo == null) return;

            PacketInfo packet = new PacketInfo();
            if (!packet.parse(data, length)) return;

            try
            {
                string[] address = endPoint.ToString().Split(':');

                App.Current.Dispatcher.Invoke(new Action(() => ServerInfo.Invoke(this, new ServerInfoEventArgs()
                {
                    Hostname = packet.HostName,
                    IP = address[0],
                    Port = int.Parse(address[1]),
                    Players = packet.Players,
                    MaxPlayers = packet.MaxPlayers,
                    Version = string.Format("{0}.{1}.{2}", packet.Major, packet.Minor, packet.Patch)
                })));
            }
            catch (Exception)
            {
                // Just ignore
            }
        }

        private void OnPacketDetails(byte[] data, int length, EndPoint endPoint)
        {
            if (ServerDetails == null) return;

            PacketDetails packet = new PacketDetails();
            if (!packet.parse(data, length)) return;

            try
            {
                string[] address = endPoint.ToString().Split(':');

                App.Current.Dispatcher.Invoke(new Action(() => ServerDetails.Invoke(this, new ServerDetailsEventArgs()
                {
                    IP = address[0],
                    Port = int.Parse(address[1]),
                    World = packet.World,
                    Description = packet.Description
                })));
            }
            catch (Exception)
            {
                // Just ignore
            }
        }
    }
}
