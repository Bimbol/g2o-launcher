﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G2O_Launcher.Network
{
    class PacketInfo
    {
        public const string Identifier = "GOi";

        public int Major { get; private set; }
        public int Minor { get; private set; }
        public int Patch { get; private set; }

        public int Players { get; private set; }
        public int MaxPlayers { get; private set; }

        public string HostName { get; private set; }

        public bool parse(byte[] data, int length)
        {
            int version = data[3];
            if (version != 0x1) return false;

            Major = data[4];
            Minor = data[5];
            Patch = data[6];
            Players = data[7];
            MaxPlayers = data[8];
            HostName = Encoding.ASCII.GetString(data, 9, length - 9);

            return true;
        }
    }
}
