﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G2O_Launcher.Network
{
    class PacketDetails
    {
        public const string Identifier = "GOd";

        public string World;
        public string Description;

        public bool parse(byte[] data, int length)
        {
            int version = data[3];
            if (version != 0x1) return false;

            int worldLen = data[4];

            World = Encoding.ASCII.GetString(data, 5, worldLen);
            Description = Encoding.ASCII.GetString(data, 5 + worldLen, length - 5 - worldLen);

            return true;
        }
    }
}
