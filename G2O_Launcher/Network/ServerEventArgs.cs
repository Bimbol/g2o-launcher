﻿namespace G2O_Launcher.Network
{
    using System;

    class ServerEventArgs : EventArgs
    {
        public string IP;
        public int Port;
    }
}
