﻿namespace G2O_Launcher.Update
{
    enum UpdateStatus
    {
        FAILED = -1,
        SUCCESS,
        OUTDATED,
        NOT_SUPPORTED,
        QUERY_ERROR,
    }
}
