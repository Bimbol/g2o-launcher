﻿namespace G2O_Launcher.Update
{
    using System;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows.Threading;
    using System.ComponentModel;
    using System.IO;
    using System.Net;
    using System.Net.Http;
    using System.Collections.Generic;
    using ICSharpCode.SharpZipLib;
    using ICSharpCode.SharpZipLib.Core;
    using ICSharpCode.SharpZipLib.Zip;
    using Newtonsoft.Json;
    using G2O_Launcher.G2O;

    class Updater
    {
        public const string UpdateExt = ".update";
        private readonly string UpdateUrl = "http://185.5.97.181:8000/update/version";

        public event EventHandler<UpdateResponseEventArgs> UpdateResult;
        public event EventHandler<UpdateProgressEventArgs> DownloadProgress;
        public event EventHandler<UpdateCompleteEventArgs> DownloadCompleted;

        private DispatcherTimer _Timer;

        public Updater()
        {
            _Timer = new DispatcherTimer();
            _Timer.Interval = TimeSpan.FromSeconds(60);
            _Timer.Tick += (obj, args) => Check();
            _Timer.IsEnabled = true;
        }

        public async void Check()
        {
            if (UpdateResult == null) return;

            try
            {
                using (var httpClient = new HttpClient())
                {
                    G2O.Version version = null;
                    using (var proxy = new Proxy())
                    {
                        version = proxy.GetVersion();
                    }

                    var jsonData = JsonConvert.SerializeObject(version);
                    var content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                    var responseHttp = await httpClient.PostAsync(UpdateUrl, content);
                    var responseBody = await responseHttp.Content.ReadAsStringAsync();

                    UpdateResponseEventArgs response = JsonConvert.DeserializeObject<UpdateResponseEventArgs>(responseBody);
                    this.UpdateResult.Invoke(this, response);
                }
            }
            catch (Exception)
            {
                UpdateResult.Invoke(this, new UpdateResponseEventArgs(UpdateStatus.FAILED));
            }
        }

        public void Download(string address, string filename)
        {
            using (var webClient = new WebClient())
            {
                try
                {
                    webClient.DownloadFileCompleted += (obj, args) =>
                    {
                        List<string> filesToUpdate = Extract(filename);
                        File.Delete(filename);

                        if (DownloadProgress != null) DownloadProgress(obj, new UpdateProgressEventArgs(100));
                        if (DownloadCompleted != null) DownloadCompleted(obj, new UpdateCompleteEventArgs(filesToUpdate));

                        _Timer.IsEnabled = true;
                    };

                    webClient.DownloadProgressChanged += (obj, args) =>
                    {
                        if (DownloadProgress != null) DownloadProgress(obj, new UpdateProgressEventArgs(args.ProgressPercentage));
                    };
                    
                    _Timer.IsEnabled = false;
                    Uri url = address.StartsWith("http://", StringComparison.OrdinalIgnoreCase) ? new Uri(address) : new Uri("http://" + address);

                    webClient.DownloadFileAsync(url, filename);
                }
                catch (Exception)
                {
                }
            }
        }

        private List<string> Extract(string archive)
        {
            ZipFile zipFile = null;
            List<string> filesToUpdate = new List<string>();

            try
            {
                FileStream fileStream = File.OpenRead(archive);
                zipFile = new ZipFile(fileStream);

                foreach (ZipEntry zipEntry in zipFile)
                {
                    if (!zipEntry.IsFile) continue;

                    String entryFileName = zipEntry.Name;

                    byte[] buffer = new byte[4096];
                    Stream zipStream = zipFile.GetInputStream(zipEntry);

                    if (Path.GetExtension(entryFileName) == UpdateExt)
                        filesToUpdate.Add(Path.GetFileNameWithoutExtension(entryFileName));

                    string directoryName = System.IO.Path.GetDirectoryName(entryFileName);
                    if (directoryName.Length > 0) Directory.CreateDirectory(directoryName);

                    using (FileStream streamWriter = File.Create(entryFileName))
                    {
                        StreamUtils.Copy(zipStream, streamWriter, buffer);
                    }
                }
            }
            catch (FileNotFoundException)
            {
            }
            finally
            {
                if (zipFile != null)
                {
                    zipFile.IsStreamOwner = true;
                    zipFile.Close();
                }
            }

            return filesToUpdate;
        }
    }
}
