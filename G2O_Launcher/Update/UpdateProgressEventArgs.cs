﻿namespace G2O_Launcher.Update
{
    using System;

    class UpdateProgressEventArgs : EventArgs
    {
        public int Progress;

        public UpdateProgressEventArgs(int progress)
        {
            Progress = progress;
        }
    }
}
