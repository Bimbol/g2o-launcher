﻿namespace G2O_Launcher.Update
{
    using System;
    using System.Collections.Generic;

    class UpdateCompleteEventArgs : EventArgs
    {
        public List<string> Files;

        public UpdateCompleteEventArgs(List<string> files)
        {
            Files = files;
        }
    }
}
