﻿namespace G2O_Launcher.Update
{
    using System;
    using G2O_Launcher.Update;

    class UpdateResponseEventArgs : EventArgs
    {
        public int Major;
        public int Minor;
        public int Patch;
        public int Build;

        public int Version;
        public string Link;
        public string Filename;
        public UpdateStatus Code;

        public UpdateResponseEventArgs(UpdateStatus code)
        {
            Code = code;
        }
    }
}
