﻿namespace G2O_Launcher.Launcher
{
    class Server : ServerBase
    {
        private int _Ping;

        public string Hostname { get; set; }
        public string Slots { get; set; }
        public string Version { get; set; }

        public int Ping
        {
            get
            {
                return _Ping;
            }

            set
            {
                if (value != _Ping)
                {
                    _Ping = value;
                    OnPropertyChanged("Ping");
                }
            }
        }
    }
}
