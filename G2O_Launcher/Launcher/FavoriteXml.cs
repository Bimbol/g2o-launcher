﻿namespace G2O_Launcher.Launcher
{
    using System;
    using System.Xml;
    using System.Windows;
    using System.Collections.Generic;
    using System.Text.RegularExpressions;
    using G2O_Launcher.Launcher;

    class FavoriteXml
    {
        public static void Generate(string path, List<Server> servers)
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlElement root = xmlDoc.CreateElement("Servers");

            foreach (var server in servers)
            {
                XmlElement nodeServer = xmlDoc.CreateElement("Server");

                XmlElement nodeIP = xmlDoc.CreateElement("Host");
                nodeIP.InnerText = server.Host;
                nodeServer.AppendChild(nodeIP);

                XmlElement nodePort = xmlDoc.CreateElement("Port");
                nodePort.InnerText = server.Port.ToString();
                nodeServer.AppendChild(nodePort);

                root.AppendChild(nodeServer);
            }

            xmlDoc.AppendChild(root);
            xmlDoc.Save(path);
        }

        public static List<Server> Parse(string path)
        {
            List<Server> servers = new List<Server>();

            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(path);

                XmlNode serversNode = xmlDoc.SelectSingleNode("Servers");
                if (serversNode == null) throw new ArgumentNullException("Servers node didn't exist.");

                Regex reHost = new Regex(@"^[a-zA-Z0-9\.\-]+$");
                XmlNodeList serversList = serversNode.SelectNodes("Server");

                foreach (XmlNode serverNode in serversList)
                {
                    string host = serverNode.SelectSingleNode("Host").InnerText;
                    int port = int.Parse(serverNode.SelectSingleNode("Port").InnerText);

                    Match match = reHost.Match(host);
                    if (!match.Success) throw new ArgumentException("Wrong IP format!");

                    servers.Add(new ServerNotFound(host, port));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "FavoriteXml Error");
            }

            return servers;
        }
    }
}
