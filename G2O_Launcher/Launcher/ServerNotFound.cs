﻿namespace G2O_Launcher.Launcher
{
    class ServerNotFound : Server
    {
        public ServerNotFound(string host, int port)
        {
            Host = host;
            Port = port;

            Hostname = "Unknown Hostname";
            Slots = "0/0";
            Version = "N/A";
            Ping = 9999;
        }
    }
}
