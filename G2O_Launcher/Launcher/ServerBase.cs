﻿namespace G2O_Launcher.Launcher
{
    using G2O_Launcher.ModelView;

    class ServerBase : BaseViewModel
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public string IP;
        public bool Reached;
    }
}
