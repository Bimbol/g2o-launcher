﻿namespace G2O_Launcher.Launcher
{
    using System;
    using System.Linq;
    using System.Collections.Generic;

    class ServersDict
    {
        private readonly string KeyPattern = "{0}:{1}";
        private Dictionary<string, Server> _Servers;

        public ServersDict()
        {
            _Servers = new Dictionary<string, Server>();
        }

        public Dictionary<string, Server>.ValueCollection Servers
        {
            get
            {
                return _Servers.Values;
            }
        }

        public Server Get(string host, int port)
        {
            string key = MakeKey(host, port);
            if (_Servers.ContainsKey(key))
                return null;

            return _Servers[key];
        }

        public Server GetByIP(string ipAddr, int port)
        {
            return _Servers.FirstOrDefault((x) => x.Value.IP == ipAddr && x.Value.Port == port).Value;
        }

        public bool Add(Server server)
        {
            if (server == null)
                throw new ArgumentNullException();

            string key = MakeKey(server.Host, server.Port);
            if (_Servers.ContainsKey(key))
                return false;

            _Servers.Add(key, server);
            return true;
        }

        public bool Remove(string host, int port)
        {
            return _Servers.Remove(MakeKey(host, port));
        }

        public bool Remove(Server server)
        {
            if (server == null)
                throw new ArgumentNullException();

            return Remove(server.Host, server.Port);
        }

        private string MakeKey(string ipAddr, int port)
        {
            return string.Format(KeyPattern, ipAddr, port);
        }
    }
}
