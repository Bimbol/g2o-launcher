﻿namespace G2O_Launcher.Utils
{
    using System.Windows.Input;

    class DynamicMenuItem
    {
        public string Header { get; set; }
        public ICommand Command { get; set; }
    }
}
