﻿namespace G2O_Launcher.G2O
{
    using System;
    using System.Runtime.InteropServices;
    using G2O_Launcher.Utils;

    class Proxy : IDisposable
    {
        public enum RunResult
        {
            MISSING_VERSION = 1,
            GOTHIC_NOT_FOUNT = 2,
            UNKNOW = 3,
        }

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        private delegate int G2O_Run(int major, int minor, int patch);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        private delegate void G2O_Version(ref int major, ref int minor, ref int patch, ref int build);

        private IntPtr _ProxyDll;
        private G2O_Run _RunFunc;
        private G2O_Version _VersionFunc;

        public Proxy()
        {
            _ProxyDll = Dll.LoadLibrary("G2O_Proxy.dll");
            if (_ProxyDll == IntPtr.Zero) throw new ArgumentNullException("G2O_Proxy.dll not found!");

            IntPtr address = IntPtr.Zero;

            address = Dll.GetProcAddress(_ProxyDll, @"G2O_Run");
            if (address == IntPtr.Zero) throw new ArgumentNullException("G2O_Run function not found in G2O_Proxy.dll!");
            _RunFunc = (G2O_Run)Marshal.GetDelegateForFunctionPointer(address, typeof(G2O_Run));

            address = Dll.GetProcAddress(_ProxyDll, @"G2O_Version");
            if (address == IntPtr.Zero) throw new ArgumentNullException("G2O_Version function not found in G2O_Proxy.dll!");
            _VersionFunc = (G2O_Version)Marshal.GetDelegateForFunctionPointer(address, typeof(G2O_Version));
        }

        ~Proxy()
        {
            Dispose();
        }

        public void Dispose()
        {
            if (_ProxyDll != IntPtr.Zero) Dll.FreeLibrary(_ProxyDll);
        }

        public G2O.Version GetVersion()
        {
            int major = 0, minor = 0, patch = 0, build = 0;
            _VersionFunc.Invoke(ref major, ref minor, ref patch, ref build);

            return new Version(major, minor, patch, build);
        }

        public RunResult Run(Version version)
        {
            return (RunResult)_RunFunc.Invoke(version.Major, version.Minor, version.Patch);
        }
    }
}
