﻿namespace G2O_Launcher.G2O
{
    using System;
    using Microsoft.Win32;

    static class RegistryConfig
    {
        private const string UserKey = "HKEY_CURRENT_USER";
        private const string MultiplayerKey = UserKey + "\\Software\\G2O";
        private const string DefaultNickname = "Nickname";
        private const string DefaultWorld = "NEWWORLD\\NEWWORLD.ZEN";

        public static string Nickname
        {
            get
            {
                string nickname = Registry.GetValue(MultiplayerKey, "nickname", DefaultNickname) as string;
                return string.IsNullOrEmpty(nickname) ? DefaultNickname : nickname;
            }

            set
            {
                Registry.SetValue(MultiplayerKey, "nickname", value);
            }
        }

        public static string IpPort
        {
            set
            {
                Registry.SetValue(MultiplayerKey, "ip_port", value);
            }
        }

        public static string World
        {
            get
            {
                string world = Registry.GetValue(MultiplayerKey, "world", DefaultWorld) as string;
                return string.IsNullOrEmpty(world) ? DefaultWorld : world;
            }

            set
            {
                Registry.SetValue(MultiplayerKey, "world", value);
            }
        }
    }
}
