﻿namespace G2O_Launcher.G2O
{
    class Version
    {
        public int Major;
        public int Minor;
        public int Patch;
        public int Build;

        public Version()
        {

        }

        public Version(int major, int minor, int patch, int build = 0)
        {
            Major = major;
            Minor = minor;
            Patch = patch;
            Build = build;
        }

        public static Version Parse(string versionStr)
        {
            string[] versionNr = versionStr.Split('.');
            if (versionNr.Length < 3 && versionNr.Length > 4) return null;

            Version version = new Version();
            version.Major = int.Parse(versionNr[0]);
            version.Minor = int.Parse(versionNr[1]);
            version.Patch = int.Parse(versionNr[2]);
            version.Build = versionNr.Length == 4 ? int.Parse(versionNr[3]) : 0;

            return version;
        }

        public override string ToString()
        {
            return string.Format("{0}.{1}.{2}.{3}", Major, Minor, Patch, Build);
        }
    }
}
