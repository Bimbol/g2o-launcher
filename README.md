# Gothic 2 Online - Launcher
Official launcher for modification Gothic 2 Online. Help us develop it.

![Scheme](img/launcher.png)

### Todos
  - Custom theme
  - Code refactoring
  - Options
  - Unit tests

### Technology
Launcher is written in .NET 4.5+ C# and uses a number of open source projects to work properly:

  - [Newtonsoft.Json](http://www.newtonsoft.com/json) - popular high-performance JSON framework
  - [SharpZipLib](https://icsharpcode.github.io/SharpZipLib/) - a Zip, GZip, Tar and BZip2 library

License
----
The code is licensed under **GNU GENERAL PUBLIC LICENSE**.

Images was created by **AlbatrOS**, and he holds all the rights.